from django.http import HttpResponse, Http404 ,HttpResponseRedirect, HttpResponseNotFound
from django.shortcuts import render_to_response,get_object_or_404
from django.template import RequestContext

from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login ,logout

from django.views.decorators.csrf import csrf_exempt ,csrf_protect

from django.db.models import Q
from django.db.models import Count
from datetime import datetime, timedelta


from django.core.paginator import Paginator, InvalidPage, EmptyPage ,PageNotAnInteger
from marking.forms import *
from marking.models import *


####################


def main_page(request):
	
	if request.user.is_authenticated():
		return HttpResponseRedirect('/user/%s/' % request.user.username)
	
	shared_bookmarks = Bookmark.objects.filter(status=True).order_by('-date')[:10]
	count_shared = Bookmark.objects.all()
	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
						
	
	

	variables = RequestContext(request, {
		'shared_bookmarks': shared_bookmarks,
		'count':count_shared.count(),
		'people':people,

	})  
	
	template = 'index.html'
 	return render_to_response(template, variables)
 	
 	
####################
@csrf_exempt
def login(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/user/%s/' % request.user.username)
		
	username = request.POST.get('username', '')
	password = request.POST.get('password', '')
	user = auth.authenticate(username=username, password=password)	
	

#login success		
	if user is not None:
		login(request,user)
		return HttpResponseRedirect('/user/%s/' % request.user.username)
			       
#invalid login			
	else:
		return render_to_response('/registration/login.html',request)
 	

####################
@login_required(login_url='/login/')
def user_page(request,username):

	#allbookmark = Bookmark.objects.all()

	if username == request.user.username :
		
		user = get_object_or_404(User, username=username)
		query = user.bookmark_set.order_by('-id')
		allbookmark = user.bookmark_set.order_by('-id')[:10]
		paginator = Paginator(query, 15)
		page = request.GET.get('page')	
		people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	
		try:		
			bookmarks = paginator.page(page)
   	
		except PageNotAnInteger:
		
			bookmarks = paginator.page(1)

		except EmptyPage:
		
			bookmarks = paginator.page(paginator.num_pages)

    	
		is_friend = Friendship.objects.filter(
			from_friend=request.user,
			to_friend=user,
			)

		variables = RequestContext(request, {
		    	'bookmarks': bookmarks,
		   	'username': username,
		    	'show_tags': True,
		    	'show_edit': username == request.user.username,
		    	'show_delete': username == request.user.username,
		    	'is_friend': is_friend,
			'people':people,
			'allbookmark':allbookmark,
		})
  		template = 'user_page.html'			
		return render_to_response(template, variables)
	else:
		return HttpResponseRedirect('/user/%s/' % request.user.username)

####################

@login_required(login_url='/login/')
def bookmark_of(request,username):
	
	if username != request.user.username :
	
		user = get_object_or_404(User, username=username)
		query = user.bookmark_set.order_by('-id')
		query = query.filter(status=True)
		paginator = Paginator(query, 5)
		page = request.GET.get('page')	
		people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	
		try:		
			bookmarks = paginator.page(page)
   	
		except PageNotAnInteger:
		
			bookmarks = paginator.page(1)

		except EmptyPage:
		
			bookmarks = paginator.page(paginator.num_pages)

    	
		is_friend = Friendship.objects.filter(
			from_friend=request.user,
			to_friend=user,
			)

		variables = RequestContext(request, {
		    	'bookmarks': bookmarks,
		   	'username': username,
		    	'show_tags': True,
		    	'show_edit': username == request.user.username,
		    	'show_delete': username == request.user.username,
		    	'is_friend': is_friend,
			'people':people,
		})
  		template = 'bookmark_of.html'			
		return render_to_response(template, variables)
	else:
		return HttpResponseRedirect('/user/%s/' % request.user.username)

####################
@login_required(login_url='/login/')
def tag_page(request, tag_name):
	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	tag = get_object_or_404(Tag, name=tag_name)
	id_user = request.user.id
	bookmarks = tag.bookmarks.order_by('-id')
	bookmarks = bookmarks.filter( Q(user_id=id_user)|Q(status=True) )
	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]

	variables = RequestContext(request, {
	    'bookmarks': bookmarks,
	    'tag_name': tag_name,
	    'show_tags': True,
	    'show_user': True,
	    'id_test': id_user,
	    'people':people,
	    'allbookmark':allbookmark,

  	})
  
	template = 'tag_page.html'
	return render_to_response(template, variables)


####################
@login_required(login_url='/login/')
def tag_cloud_page(request):

	user = request.user
	allbookmark = user.bookmark_set.order_by('-id')[:10]
	MAX_WEIGHT = 10
	tags = Tag.objects.order_by('name')
	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
  # Calculate tag, min and max counts.
	min_count = max_count = tags[0].bookmarks.count()
	for tag in tags:
		tag.count = tag.bookmarks.count()
		if tag.count < min_count:
			min_count = tag.count
		if max_count < tag.count:
			max_count = tag.count

  # Calculate count range. Avoid dividing by zero.
	range = float(max_count - min_count)
	if range == 0.0:
		range = 1.0

  # Calculate tag weights.
	for tag in tags:
		tag.weight = int(MAX_WEIGHT * (tag.count - min_count) / range)

		variables = RequestContext(request, {
		'tags': tags,
		'people':people,
		'allbookmark':allbookmark,
		})
  
	template = 'tag_cloud_page.html'
	return render_to_response(template, variables)
  
####################

def logout_page(request):
	try:
		logout(request)
	except KeyError:
		pass
		
	return HttpResponseRedirect('/')
####################

def register_page(request):
	
	if request.method == 'POST':
		form = RegistrationForm(request.POST)		
#################################################
		if form.is_valid():
			user = User.objects.create_user(
				username=form.cleaned_data['username'],
				password=form.cleaned_data['password2'],
				email=form.cleaned_data['email'],         
			)
			user.first_name = form.cleaned_data['first_name']
			user.last_name = form.cleaned_data['last_name']
			user.save()
			
			if 'invitation' in request.session:
				# Retrieve the invitation object.
				invitation = Invitation.objects.get(id=request.session['invitation'])
				# Create frienship from user to sender.
				friendship = Friendship(
					from_friend=user,
					to_friend=invitation.sender
				)
				friendship.save()
				# Create friendship from sender to user.
				friendship = Friendship(
					from_friend=invitation.sender,
					to_friend=user
				)
				friendship.save()
				# Delete the invitation from the database and session.
				invitation.delete()
				del request.session['invitation']  
				
			return HttpResponseRedirect('/register/success/')	
					
#################################################	
		
	else:
		form = RegistrationForm()
		
	count_shared = SharedBookmark.objects.all()	
		
	variables = RequestContext(request, {
    'form': form,
    'count': count_shared.count(),
	})
	template = 'registration/register.html'	
	return render_to_response(template, variables)


####################

def register_success(request):
	template = 'registration/register_success.html'
	return render_to_response(template, RequestContext(request))


####################
@login_required(login_url='/login/')
def bookmark_page(request, bookmark_id):
	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	shared_bookmark = get_object_or_404(
		SharedBookmark,
		id=bookmark_id,		
		)
	bookmark = get_object_or_404(
		Bookmark,
		id=bookmark_id,		
		)
	variables = RequestContext(request, {
		'shared_bookmark': shared_bookmark,
		'bookmark': bookmark,
		'people':people,
		'allbookmark':allbookmark,
		're_to':bookmark_id,
		})
	template = 'bookmark_page.html'
	return render_to_response('bookmark_page.html', variables)

####################
@login_required(login_url='/login/')
def mycomment(request, wrong_id):

	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	q = SharedBookmark.objects.get(bookmark_id=wrong_id)
	qid = (q.id)
	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]

	shared_bookmark = get_object_or_404(
		SharedBookmark,
		id= qid,		
		)
	bookmark = get_object_or_404(
		Bookmark,
		id=qid,		
		)
	variables = RequestContext(request, {
		'shared_bookmark': shared_bookmark,
		'bookmark': bookmark,
		'people':people,
		'allbookmark':allbookmark,
		're_to':qid,
		})
	template = 'bookmark_page.html'
	return render_to_response('bookmark_page.html', variables)


####################
def _bookmark_save(request, form):
	
	### Create or get link.
	link, dummy = Link.objects.get_or_create(url=form.cleaned_data['url'])
  
	### Create or get bookmark.
	bookmark, created = Bookmark.objects.get_or_create(user=request.user,link=link)
  
	### Update bookmark title.
	bookmark.title = form.cleaned_data['title']
  
	### If the bookmark is being updated, clear old tag list.
	if not created:
		bookmark.tag_set.clear()
    
	### Create new tag list.
	tag_names = form.cleaned_data['tags'].split()
	for tag_name in tag_names:
		tag, dummy = Tag.objects.get_or_create(name=tag_name)
		bookmark.tag_set.add(tag)
    
	### Share on the main page if requested.
	shared_bookmark, created = SharedBookmark.objects.get_or_create(bookmark=bookmark)
	if form.cleaned_data['share']:
		bookmark.status = True
		bookmark.shared_state = True
		shared_bookmark.status = True
		# owner_id = bookmark.user_id
		#shared_bookmark.owner_id = owner_id

		if created:
			shared_bookmark.users_voted.add(request.user)
			
		shared_bookmark.save()
  	else:
		bookmark.status = False
		bookmark.shared_state = False
		shared_bookmark.status = False
		shared_bookmark.save()



	### Save bookmark to database and return it.
	bookmark.save()
	return bookmark



####################
@login_required(login_url='/login/')
def bookmark_save_page(request):
  realuser = request.user
  allbookmark = realuser.bookmark_set.order_by('-id')[:10]
  people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
  ajax = request.GET.has_key('ajax')
  if request.method == 'POST':
    form = BookmarkSaveForm(request.POST)
    if form.is_valid():
      bookmark = _bookmark_save(request, form)
      if ajax:
        variables = RequestContext(request, {
          'bookmarks': [bookmark],
          'show_edit': True,
          'show_tags': True,
	  
          })
        return render_to_response('bookmark_list.html', variables)
      else:
      	
        return HttpResponseRedirect('/user/%s/' % request.user.username)
    else:
      if ajax:
        return HttpResponse('failure')  
  elif request.GET.has_key('url'):
    url = request.GET['url']
    title = ''
    tags = ''
    try:
      link = Link.objects.get(url=url)
      bookmark = Bookmark.objects.get(link=link, user=request.user)
      title = bookmark.title
      tags = ' '.join(tag.name for tag in bookmark.tag_set.all())
    except ObjectDoesNotExist:
      pass
    form = BookmarkSaveForm({
      'url': url,
      'title': title,
      'tags': tags
    })
  else:
    form = BookmarkSaveForm()

  variables = RequestContext(request, {
    'form': form,
    'people':people,
    'allbookmark':allbookmark,
  })
  if ajax:
    return render_to_response(
      'bookmark_save_form.html',
      variables
    )
  else:
    return render_to_response(
      'bookmark_save.html',
      variables
    )

####################
@login_required(login_url='/login/')
def bookmark_delete(request):
	if request.GET.has_key('url'):
		url = request.GET['url']
		link = Link.objects.get(url=url)
		bookmark = Bookmark.objects.get(link=link, user=request.user)
		bookmark.delete()
	return HttpResponseRedirect('/user/%s/' % request.user.username)

####################
@login_required(login_url='/login/')
def bookmark_vote_page(request):
	if request.GET.has_key('id'):
		try:
			id = request.GET['id']
			shared_bookmark = SharedBookmark.objects.get(id=id)
			user_voted = shared_bookmark.users_voted.filter(username=request.user.username)
			
			if not user_voted:
				shared_bookmark.votes += 1
				shared_bookmark.users_voted.add(request.user)
				shared_bookmark.save()
		except ObjectDoesNotExist:
			raise Http404('Bookmark not found.')
	if request.META.has_key('HTTP_REFERER'):
		return HttpResponseRedirect(request.META['HTTP_REFERER'])
	return HttpResponseRedirect('/')
	
	
####################
@login_required(login_url='/login/')
def popular_page(request):
	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	today = datetime.today()
	yesterday = today - timedelta(1)
	shared_bookmarks = SharedBookmark.objects.filter(date__gt=yesterday)
	shared_bookmarks = SharedBookmark.objects.filter(status=True)
	shared_bookmarks = shared_bookmarks.order_by('-votes')[:15]

	variables = RequestContext(request, {
		'shared_bookmarks': shared_bookmarks,
		'people':people,
		'allbookmark':allbookmark,
		'realuser' : realuser,
	})
	template = 'hot_url.html'
	return render_to_response(template, variables)





####################
@login_required(login_url='/login/')
def search_page(request):
	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	bookmarks = []
	show_results = False

	if request.method == 'GET':
		query = "%s"%request.GET['q']
		show_results = True
		query = request.GET['q'].strip()
		if query:
			keywords = query.split()
			q = Q()
			for keyword in keywords:
				q = q & Q(title__icontains=keyword)

			
	bookmarks = Bookmark.objects.filter(q,Q(user_id=realuser)|Q(status=True))
	count_shared = SharedBookmark.objects.all()
	variables = RequestContext(request, {
		
		'bookmarks': bookmarks,
		'show_results': show_results,
		'show_tags': True,
		'show_user': True,
		'count':count_shared.count(),
		'people':people,
		'allbookmark':allbookmark,
	})
	
	if request.GET.has_key('ajax'):
		if request.user.is_authenticated():	
			return render_to_response('search.html', variables)
		else:
			return render_to_response('search_result.html', variables)
		
	else:
		if request.user.is_authenticated():
			return render_to_response('search.html', variables)
		else:
			return render_to_response('search_result.html', variables)

####################
def search_indexpage(request):
	
	bookmarks = []
	show_results = False

	if request.method == 'GET':
		query = "%s"%request.GET['q']
		show_results = True
		query = request.GET['q'].strip()
		if query:
			keywords = query.split()
			q = Q()
			for keyword in keywords:
				q = q & Q(title__icontains=keyword)

			
	bookmarks = Bookmark.objects.filter(q,status=True)
	count_shared = SharedBookmark.objects.all()
	variables = RequestContext(request, {
		
		'bookmarks': bookmarks,
		'show_results': show_results,
		'show_tags': True,
		'show_user': True,
		'count':count_shared.count(),
	})
	
	if request.GET.has_key('ajax'):
		if request.user.is_authenticated():	
			return render_to_response('search_index.html', variables)
		else:
			return render_to_response('search_result_index.html', variables)
		
	else:
		if request.user.is_authenticated():
			return render_to_response('search_index.html', variables)
		else:
			return render_to_response('search_result_index.html', variables)

####################

def ajax_tag_autocomplete(request):
	
	if request.GET.has_key('q'):
		tags = Tag.objects.filter(name__istartswith=request.GET['q'])[:10]
		return HttpResponse('\n'.join(tag.name for tag in tags))

	return HttpResponse()

##############################
@login_required(login_url='/login/')
def userprofile( request,username):
	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	

	user = get_object_or_404(User, username=username)
	friends_list = [friendship.to_friend for friendship in user.friend_set.all()]
	follow_me_list = [friendship.from_friend for friendship in user.to_friend_set.all()]
	
	friends_set = user.friend_set.all()
	to_friends_set = user.to_friend_set.all()	
	
	is_friend = Friendship.objects.filter(
			from_friend=request.user,
			to_friend=user,
			)

	#people = Friendship.objects.filter(
	#		~Q(from_friend=request.user),
	#		~Q(to_friend=request.user)
	#		)
	
	#people = [friendship.to_friend for friendship in people]
	#people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]

	if username == request.user.username:
		bookmarks_anno = user.bookmark_set.order_by('-id')[:10]
		bookmarks = user.bookmark_set.order_by('-id')
		
	else:
		
		bookmarks_anno = user.bookmark_set.filter(status=True).order_by('-id')[:10]
		bookmarks = user.bookmark_set.filter(status=True).order_by('-id')


	variables = RequestContext(request, {
		'following_count':friends_set.count(),  		#followings
		'follower_count':to_friends_set.count(),  #followers
		'friend_list':friends_list,
		'follow_me_list':follow_me_list,  
		'count':bookmarks.count(),
		'username':user,
		'bookmarks': bookmarks,
		'bookmarks_anno': bookmarks_anno,
		'show_edit': username == request.user.username,
		'is_friend':is_friend,
		'fakeuser':user.username,
		'people':people,
		'allbookmark':allbookmark,

	})
	
	template = 'profile.html'
	return render_to_response(template,variables)


##############################
@login_required(login_url='/login/')
def edit_userprofile( request,username):
	
	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]

	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	user = get_object_or_404(User, username=username)
	friends_set = user.friend_set.all()
	to_friends_set = user.to_friend_set.all()
	bookmarks = user.bookmark_set.order_by('-id')


	
	if request.method == 'POST':
		
		user.first_name = request.POST['first_name']
		user.last_name = request.POST['last_name']
		user.save()
		

	else:
		variables = RequestContext(request, {    
    		'username':user,
    		'friend_set':friends_set.count(),
    		'to_friend_set':to_friends_set.count(),
    		'count':bookmarks.count(),	
		'people':people,
		'allbookmark':allbookmark,	
		})
		template = 'profile_edit.html'
		return render_to_response(template,variables)
		
	variables = RequestContext(request, {    
    		'username':user,
    		'friend_set':friends_set.count(),
    		'to_friend_set':to_friends_set.count(),
    		'count':bookmarks.count(),	
		'allbookmark':allbookmark,			
	})
	
	return HttpResponseRedirect('/profile/%s/' % request.user.username)


##############################



def ChangePassword( request, username):
	
	changepassword = ChangePasswordForm()
	u = User.objects.get(username__exact=username)
	u.set_password('new password')
	
	
###################################	

def friends_page(request, username):
	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	user = get_object_or_404(User, username=username)
	friends = [friendship.to_friend for friendship in user.friend_set.all()]
	friend_bookmarks = Bookmark.objects.filter(user__in=friends).order_by('-id')
	friend_bookmarks = friend_bookmarks.filter(status=True)
	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]

	paginator = Paginator(friend_bookmarks, 15)
	page = request.GET.get('page')
	
	try:		
		friend_bookmarks = paginator.page(page)
   	
	except PageNotAnInteger:
		
		friend_bookmarks = paginator.page(1)

	except EmptyPage:
		
		friend_bookmarks = paginator.page(paginator.num_pages)

	variables = RequestContext(request, {
		'username': username,
		'friends': friends,
		'bookmarks': friend_bookmarks,
		'show_tags': True,
		'show_user': True,
		'people':people,
		'allbookmark':allbookmark,
	})
	return render_to_response('friends_page.html', variables)	
	
	
	
###################################	

@login_required(login_url='/login/')
def friend_add(request):
	
	if request.GET.has_key('username'):
		friend = get_object_or_404(User, username=request.GET['username'])
		friendship = Friendship(
			from_friend=request.user,
			to_friend=friend
		)
		friendship.save()		
		return HttpResponseRedirect('/profile/%s/' % friend)
	else:
		raise Http404
	
	
###################################

#@login_required
#def friend_invite(request):
#	if request.method == 'POST':
#		if form.is_valid():
#			invitation = Invitation(
#				name = form.cleaned_data['name'],
#				email = form.cleaned_data['email'],
#				code = User.objects.make_random_password(20),
#				sender = request.user
#			)
#			
#			invitation.save()
#			try:
#				invitation.send()
#				request.user.message_set.create(
#					message='An invitation was sent to %s.' % invitation.email
##			except:
#				request.user.message_set.create(
#					message='There was an error while sending the invitation.'
#				)
#			return HttpResponseRedirect('/friend/invite/')
#	else:
#		form = FriendInviteForm()
#
#	variables = RequestContext(request, {
#		'form': form
#	})
#	return render_to_response('friend_invite.html', variables)
#

#def friend_accept(request, code):
#	invitation = get_object_or_404(Invitation, code__exact=code)
#	request.session['invitation'] = invitation.id
#	return HttpResponseRedirect('/register/')
@login_required(login_url='/login/')	
def following(request, username):
	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	user = get_object_or_404(User, username=username)
	friends = [friendship.to_friend for friendship in user.friend_set.all()]
	friend_bookmarks = Bookmark.objects.filter(user__in=friends).order_by('-id')
	
	friends_count = user.friend_set.all()

	variables = RequestContext(request, {
		'count':friends_count.count(),
		'username': username,
		'friends': friends,
		'bookmarks': friend_bookmarks[:10],
		'show_tags': True,
		'show_user': True,
		'show_unfollow':username == request.user.username,
		'people':people,
		'allbookmark':allbookmark,
	})
	return render_to_response('following.html', variables)	
	
	
	
###################################	
@login_required(login_url='/login/')
def follower(request, username):
	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	user = get_object_or_404(User, username=username)
	friends = [friendship.from_friend for friendship in user.to_friend_set.all()]
	friend_bookmarks = Bookmark.objects.filter(user__in=friends).order_by('-id')

	friends_count = user.to_friend_set.all()	

	variables = RequestContext(request, {
		'count':friends_count.count(),
		'username': username,
		'friends': friends,
		'bookmarks': friend_bookmarks[:10],
		'show_tags': True,
		'show_user': True,
		'people':people,
		'allbookmark':allbookmark,
	})
	return render_to_response('follower.html', variables)	
	
	
	
###################################	
@login_required(login_url='/login/')
def friend_delete(request):
	
	if request.GET.has_key('username'):
		friend = get_object_or_404(User, username=request.GET['username'])
		friendship = Friendship.objects.get(from_friend=request.user,to_friend=friend)		
		friendship.delete()
		
	
	return HttpResponseRedirect('/following_list/%s/' % request.user.username)

###################################
@login_required(login_url='/login/')
def rank(request):
	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	
	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	people2 = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')
	user = request.user
	myid = user.id
	friends_list = [friendship.to_friend for friendship in request.user.friend_set.all()]

	variables = RequestContext(request, {
		'people':people,
		'people2':people2,
		'myid' : myid,
		'myfriend_list':friends_list,
		'allbookmark':allbookmark,
		})
	template = 'rank.html'

	return render_to_response(template, variables)	

###################################	

@login_required(login_url='/login/')
def friend_add_rank(request):
	
	if request.GET.has_key('username'):
		friend = get_object_or_404(User, username=request.GET['username'])
		friendship = Friendship(
			from_friend=request.user,
			to_friend=friend
		)
		friendship.save()		
		return HttpResponseRedirect('/rank/')
	else:
		raise Http404

##############################
@login_required(login_url='/login/')
def postcomment( request ):
	
	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]

	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
		
	variables = RequestContext(request, {    
		'allbookmark':allbookmark,
		'people':people,			
	})
	
	template = 'posted.html'
	return render_to_response(template, variables)
##############################
@login_required(login_url='/login/')
def test( request ):
	
	realuser = request.user
	allbookmark = realuser.bookmark_set.order_by('-id')[:10]

	people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
		
	variables = RequestContext(request, {    
		'allbookmark':allbookmark,
		'people':people,			
	})
	
	template = 'test.html'
	return render_to_response(template, variables)

################################################# TESTING ######################################################

###########CLEAR BOOKMARK BY USER##################
@login_required(login_url='/login/')
def test_clearbookmark(request,username):


	if username == request.user.username :
		
		user = get_object_or_404(User, username=username)
		query = user.bookmark_set.order_by('-id')
		
		for bookmark in query:
			delete_it = Bookmark.objects.get(link=bookmark.link, user=user)
			delete_it.delete()

		variables = RequestContext(request, {
		    	'bookmarks': query,
		   	'username': username,

		})
  		template = 'test_clearbookmark.html'			
		return render_to_response(template, variables)
	else:
		return HttpResponseRedirect('/user/%s/' % request.user.username)

###########CHECK REGISTERAION BY USER##################
def test_checkregister(request,username):
		name = ''
		text = ''
		
		if User.objects.filter(username=username):
			name = username
			text = ' has found in system.'
		else:
			name = username
			text = ' The username that your request never register.'
			

		variables = RequestContext(request, {
		   	'name': name,
			'text' : text,

		})

  		template = 'test_checkregister.html'			
		return render_to_response(template, variables)

################### TEST SEARCHING  ######################
@login_required(login_url='/login/')
def test_searching(request):

	realuser = request.user
	#allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	#people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	#bookmarks = []
	#show_results = False

	if request.method == 'GET':
		query = "%s"%request.GET['q']
		show_results = True
		query = request.GET['q'].strip()
		if query:
			keywords = query.split()
			q = Q()
			for keyword in keywords:
				q = q & Q(title__icontains=keyword)

			
	bookmarks = Bookmark.objects.filter(q,Q(user_id=realuser)|Q(status=True))
	private_bookmarks = Bookmark.objects.filter(q,Q(status=False))
	public_bookmarks = Bookmark.objects.filter(q,Q(status=True))
	your_bookmarks = Bookmark.objects.filter(q,Q(user_id=realuser))
	yousee_bookmarks = Bookmark.objects.filter(q,Q(user_id=realuser)|Q(status=True))
	#count_shared = SharedBookmark.objects.all()
	variables = RequestContext(request, {
		
		'bookmarks': bookmarks,
		'private_bookmarks':private_bookmarks,
		'public_bookmarks':public_bookmarks,
		'your_bookmarks':your_bookmarks,
		'yousee_bookmarks':yousee_bookmarks,
		#'show_results': show_results,
		#'show_tags': True,
		#'show_user': True,
		#'count':count_shared.count(),
		#'people':people,
		#'allbookmark':allbookmark,
	})

	return render_to_response('test_searching.html', variables)

################### TEST SEARCHING  ######################

def test_nosearching(request):


	bookmarks = []
	show_results = False

	if request.method == 'GET':
		query = "%s"%request.GET['q']
		show_results = True
		query = request.GET['q'].strip()
		if query:
			keywords = query.split()
			q = Q()
			for keyword in keywords:
				q = q & Q(title__icontains=keyword)

			
	bookmarks = Bookmark.objects.filter(q,status=True)
	count_shared = SharedBookmark.objects.all()
	variables = RequestContext(request, {
		
		'yousee_bookmarks': bookmarks,
		'show_results': show_results,
		'show_tags': True,
		'show_user': True,
		'count':count_shared.count(),
	})

	return render_to_response('test_nosearching.html', variables)

################### TEST ADDING  ######################
@login_required(login_url='/login/')
def test_adding(request):

	realuser = request.user
	#allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	#people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	#bookmarks = []
	#show_results = False



			
	bookmarks = Bookmark.objects.filter(user=request.user)
	variables = RequestContext(request, {
		
		'bookmarks': bookmarks,

	})

	return render_to_response('test_adding.html', variables)

################### TEST EDIT ######################
@login_required(login_url='/login/')
def test_edit(request):

	realuser = request.user
	#allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	#people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	#bookmarks = []
	#show_results = False



			
	bookmarks = Bookmark.objects.filter(user=request.user)
	variables = RequestContext(request, {
		
		'bookmarks': bookmarks,
		'show_tags': True,

	})

	return render_to_response('test_edit.html', variables)

################### TEST DELETE ######################
@login_required(login_url='/login/')
def test_delete(request):

	realuser = request.user
	#allbookmark = realuser.bookmark_set.order_by('-id')[:10]
	#people = User.objects.annotate(num_friend=Count('to_friend_set')).order_by('-num_friend')[:10]
	#bookmarks = []
	#show_results = False



			
	bookmarks = Bookmark.objects.filter(user=request.user)
	variables = RequestContext(request, {
		
		'bookmarks': bookmarks,
		'show_tags': True,

	})

	return render_to_response('test_delete.html', variables)

################### TEST VOTE ######################
@login_required(login_url='/login/')
def test_vote(request):

	realuser = request.user
	shared_bookmarks = SharedBookmark.objects.filter(status=True)
	shared_bookmarks = shared_bookmarks.order_by('-votes')
	#voter = shared_bookmarks.votes.userid

	variables = RequestContext(request, {
		'realuser': realuser,	
		'shared_bookmarks': shared_bookmarks,

	})

	return render_to_response('test_vote.html', variables)

##################################### TEST IS FRIENDS ####################################
@login_required(login_url='/login/')
def test_isfriend( request,username):
	
	user = get_object_or_404(User, username=username)
	is_friend = Friendship.objects.filter(
			from_friend=request.user,
			to_friend=user,
			)


	variables = RequestContext(request, {

		'is_friend':is_friend,

	})
	
	template = 'test_isfriend.html'
	return render_to_response(template,variables)

##################################### TEST IS FRIENDS ####################################
@login_required(login_url='/login/')
def test_comment( request,idbk):
	
	
	shared_bookmark = get_object_or_404(
		SharedBookmark,
		id=idbk,		
		)
	bookmark = get_object_or_404(
		Bookmark,
		id=idbk,		
		)
	
	variables = RequestContext(request, {
		'shared_bookmark': shared_bookmark,
		'bookmark': bookmark,

		})
	template = 'bookmark_page.html'
	return render_to_response('test_comment.html', variables)

##################################### TEST IS FRIENDS ####################################
@login_required(login_url='/login/')
def test_isvoted( request,bookmarkid):
	
	#user = get_object_or_404(User, username=username)
	#is_vote = users_voted.filter(
	#		user_id=request.user.id,
	#		sharedbookmark_id=bookmarkid,
	#		)
	shared_bookmark = SharedBookmark.objects.get(id=bookmarkid)
	is_vote = shared_bookmark.users_voted.filter(
			username=request.user.username,
		#	sharedbookmark_id=bookmarkid,
			)

	variables = RequestContext(request, {

		'is_voted':is_vote,

	})
	
	template = 'test_isvoted.html'
	return render_to_response(template,variables)


