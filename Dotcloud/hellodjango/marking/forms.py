import re
from django import forms
from django.contrib.auth.models import User


class RegistrationForm(forms.Form):
	
	username = forms.CharField(
		label='Username:: ',
		max_length=30
	)
	email = forms.EmailField(
		label='Email:: '
	)  
	first_name = forms.CharField(
		label='First name:: ', 
		max_length=30,
		required = True,
	)
	last_name = forms.CharField(
		label='Last name:: ',
		max_length=30,
		required = True,
	)  
	password1 = forms.CharField(
		label='Password:: ',
		min_length=5,
		widget=forms.PasswordInput()
	)
	password2 = forms.CharField(
		label='Password (Again):: ',
		min_length=5,
		widget=forms.PasswordInput()
	)
  
	
	def clean_username(self):
		username = self.cleaned_data['username']
    
		if len(username) < 5 :
			raise forms.ValidationError('Username must more than 4 characters.')
   
   ## Chk Username ##
   
		if not re.search(r'^\w+$', username) :
			raise forms.ValidationError('Username can only contain alphanumeric characters and underscore.')
		try:
			User.objects.get(username=username)
		except:
			return username
		raise forms.ValidationError('Username is already taken.')
    
    ## Chk Email ##
	def clean_email(self): 
		email = self.cleaned_data['email'] 
    
		if re.search(r'^\w+$', email) :
			raise forms.ValidationError('.')
		try:
			User.objects.get(email=email)
		except:
			return email
		raise forms.ValidationError('Email is already taken.')

		 ## Chk Password ##
	def clean_password2(self):
		if 'password1' in self.cleaned_data:
			password1 = self.cleaned_data['password1']
			password2 = self.cleaned_data['password2']
			if password1 == password2:
				return password2
		raise forms.ValidationError('Passwords do not match.')
    
      ## Chk FirstName ##
	def clean_firstname(self):
		if 'first_name' in self.cleaned_data:
			first_name = self.cleaned_data['first_name']	   
			return first_name
		
		 ## Chk LastName ##
	def clean_lastname(self):
		if 'last_name' in self.cleaned_data:
			first_name = self.cleaned_data['last_name']	   
			return last_name		
	
###################################

class BookmarkSaveForm(forms.Form):
	url = forms.URLField(
		label='URL ::',
		widget=forms.TextInput(attrs={'size': 64})
	)
	title = forms.CharField(
		label='Title ::',
		widget=forms.TextInput(attrs={'size': 64})
	)
	tags = forms.CharField(
		label='Tags ::',
		required=False,
		widget=forms.TextInput(attrs={'size': 64})
	)
	share = forms.BooleanField(
		label='Share on the main page',
		required=False
	)
###################################



###################################

class ChangePasswordForm(forms.Form):
	
	password1 = forms.CharField(
		label='Password:: ',
		widget=forms.PasswordInput()
  )
	password2 = forms.CharField(
		label='Password (Again):: ',
		widget=forms.PasswordInput()
  )
  
	def clean_password2(self):
		if 'password1' in self.cleaned_data:
			password1 = self.cleaned_data['password1']
			password2 = self.cleaned_data['password2']
			if password1 == password2:
				return password2
				
				
###################################


#class FriendInviteForm(forms.Form):
##	name = forms.CharField(
#		label='Friend\'s Name'
#	)
#	email = forms.EmailField(
#		label='Friend\'s Email'
#	)









			
	