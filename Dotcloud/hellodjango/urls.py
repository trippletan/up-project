import os.path
from django.conf import settings
from django.conf.urls import *

from marking.views import *
from django.views.generic.simple import direct_to_template
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()



urlpatterns = patterns('',
  

## Page ## 
	(r'^$', main_page),
	(r'^user/(\w+)/$', user_page),
	(r'^bookmark_of/(\w+)/$', bookmark_of),
	(r'tag/([^\s]+)/$', tag_page),
	(r'tag/$', tag_cloud_page),
	(r'^search/$', search_page),
	(r'^search_index/$',search_indexpage),
  	(r'^hot/$', popular_page),
 	(r'^profile/(\w+)/$', userprofile),
 	(r'^edit/(\w+)/$', edit_userprofile),
  	(r'^bookmark/(\d+)/$', bookmark_page),
  	(r'^following_list/(\w+)/$', following),
	(r'^follower_list/(\w+)/$', follower),
	(r'^mycomment/(\d+)/$', mycomment),
	(r'^rank/$', rank),
	(r'^rank/add/$', friend_add_rank),
	(r'^test/$', test),
	(r'^accounts/profile/$', re_aflogin),
	(r'^comments/posted/$', redirect_comment),
	
## Friend ##	
  	(r'^friends/(\w+)/$', friends_page),
  	(r'^friend/add/$', friend_add),
  	(r'^friend/delete/$', friend_delete),
## Session ##
  	(r'^login/$', 'django.contrib.auth.views.login'), #'django.contrib.auth.views.login'
  	(r'^logout/$', logout_page),
  	
  	(r'^register/$', register_page),
  	(r'^register/success/$',register_success),
## Content ##
  	(r'^save/$', bookmark_save_page),
	(r'^delete/$', bookmark_delete),
  	(r'^vote/$', bookmark_vote_page),
  	(r'^comments/', include('django.contrib.comments.urls')),
## Ajax ##
	(r'^ajax/tag/autocomplete/$', ajax_tag_autocomplete),
  
## Friends ##  
	(r'^friends/(\w+)/$', friends_page),
  	(r'^friend/add/$', friend_add),
## Admin ##
  	(r'^admin/', include(admin.site.urls)),

## ForTesting ##
	(r'^test_clearbookmark/(\w+)/$', test_clearbookmark),
	(r'^test_checkregister/(\w+)/$', test_checkregister),
	(r'^test_searching/$', test_searching),
	(r'^test_nosearching/$', test_nosearching),
	(r'^test_adding/$', test_adding),
	(r'^test_edit/$', test_edit),
	(r'^test_delete/$', test_delete),
	(r'^test_vote/$', test_vote),
	(r'^test_isfriend/(\w+)/$', test_isfriend),
	(r'^test_comment/(\w+)/$', test_comment),
	(r'^test_isvoted/(\w+)/$', test_isvoted),
	(r'^test_privacy/$', test_privacy),
)

#urlpatterns += patterns('django.views.generic.sample',
#&nbsp (r'^/login/$' , 'direct_to_template',{'template':'base_login.html'}),)


###  MEDIA ###
if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.STATIC_DOC_ROOT}),    
)

