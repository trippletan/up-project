from django.db import models
from django.contrib.auth.models import User
from django.template import Context
from django.db.models.signals import post_save
from django.conf import settings
from django.template.loader import get_template
from django.core.mail import send_mail

#from mptt.models import MPTTModel, TreeForeignKey

from django.contrib import admin
##
class Link(models.Model):
	url = models.URLField(unique=True)

	def __str__(self):
		return self.url
	class Admin:
		pass
##
class Bookmark(models.Model):
	
	title = models.CharField(max_length=200)
	user = models.ForeignKey(User)
	link = models.ForeignKey(Link)
	date_add = models.DateTimeField(auto_now_add=True)
	status = models.BooleanField()
	shared_state = models.BooleanField()
	def __str__(self):
		return '%s, %s' % (self.user.username, self.link.url)
		
	def get_absolute_url(self):
		return self.link.url 
		
	class Admin:
		list_display = ('title', 'link','user')
		list_filter = ('user', )
		ordering = ('title', )
		search_fields = ('title', )

###
#class UserProfile(models.Model):
#	user = models.OneToOneField(User)	
#	display = models.ImageField(upload_to='profile_picture',blank=True, null=True)


	
    	
##
class Tag(models.Model):
	name = models.CharField(max_length=64, unique=True)
	bookmarks = models.ManyToManyField(Bookmark)

	def __str__(self):
		return self.name
	class Admin:
		pass
##
class SharedBookmark(models.Model):
	bookmark = models.ForeignKey(Bookmark, unique=True)
	date = models.DateTimeField(auto_now_add=True)
	votes = models.IntegerField(default=1)
	users_voted = models.ManyToManyField(User)
	status = models.BooleanField()
	#owner_id = models.IntegerField()
	

	def __str__(self):
		return '%s, %s' % self.bookmark, self.votes
    
	class Admin:
		pass
		
#############################

class Friendship(models.Model):
	from_friend = models.ForeignKey(User, related_name='friend_set')
	to_friend = models.ForeignKey(User, related_name='to_friend_set')
  
	def __str__(self):
		return '%s, %s' % (self.from_friend.username, self.to_friend.username)

	class Admin:
		pass

	class Meta:
		unique_together = (('to_friend', 'from_friend'), )

#############################

class Invitation(models.Model):
	name = models.CharField(max_length=50)
	email = models.EmailField()
	code = models.CharField(max_length=20)
	sender = models.ForeignKey(User)

	def __str__(self):
		return '%s, %s' % (self.sender.username, self.email)

	class Admin:
		pass

	def send(self):
		subject = 'Invitation to join Django Bookmarks'
		link = 'http://%s/friend/accept/%s/' % (
			settings.SITE_HOST,
			self.code
		)
		template = get_template('invitation_email.txt')
		context = Context({
			'name': self.name,
			'link': link,
			'sender': self.sender.username,
		})
		message = template.render(context)
		send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [self.email])


class Post(models.Model):
	
	text = models.TextField()
	added  = models.DateTimeField(auto_now_add=True)

#class Comment(MPTTModel):
	
#	post = models.ForeignKey(Post)
#	author = models.ForeignKey(User)
#	comment = models.TextField()
#	added  = models.DateTimeField(auto_now_add=True)
    # a link to comment that is being replied, if one exists
#	parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

#	class MPTTMeta:
        # comments on one level will be ordered by date of creation
#		order_insertion_by=['added']



